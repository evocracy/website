(function(){

    //const names = [ 'Autocrats', 'Conspiracists', 'Propagandists', 'Dictators', 'Lobbyists', 'Trolls', 'Bots' ];
    const names = [ 'Democrats', 'Scientists', 'Journalists', 'Activists', 'Leaders', 'Entrepreneurs', 'Employees' ];
    /*const names = {
        'Democrats': 'love',
        'Autocrats': 'hate',
        'Scientists': 'love',
        'Conspiracists': 'hate',
        'Journalists': 'love',
        'Propagandists': 'hate',
        'Activists': 'love',
        'Dictators': 'hate',
        'Leaders': 'love',
        'Lobbyists': 'hate',
        'Entrepreneurs': 'love',
        'Trolls': 'hate',
        'Employees': 'love',
        'Bots': 'hate'
    };*/
    
    const colors = [ '#CE93D8', '#80DEEA', '#F48FB1', '#80CBC4', '#BCAAA4', '#EF9A9A' ];
    let i = 0;
    let j = 0;

    setInterval(() => {
        $('.first-word').fadeOut(function() {
        //$('.change-word').fadeOut(function() {
            i = (i+1)%names.length;
            //i = (i+1)%Object.keys(names).length;
            j = (j+1)%colors.length;
            $(this).html(names[i]).fadeIn();
            //$('.first-word').html(Object.keys(names)[i]).fadeIn();
            //$('.verb-word').html(Object.values(names)[i]).fadeIn();
            $(this).css('color', colors[j]);
            //$('.change-word').css('color', colors[j]);
        });
    }, 2500);
    
    /* Copy span to clipboard */
    
    const span = document.querySelectorAll('span.select-address');
    
    span.forEach(item => {
        item.onclick = function(e) {
            document.execCommand("copy");
            $('.snackbar-clipboard').fadeIn(() => {
                $('.snackbar-clipboard').delay(2500).fadeOut();
            });
        }
        
        item.addEventListener("copy", function(event) {
            event.preventDefault();
            if (event.clipboardData) {
                event.clipboardData.setData("text/plain", span.textContent);
                console.log(event.clipboardData.getData("text"))
            }
        });
    });

})();
